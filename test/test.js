var expect = require('chai').expect,
  assert = require('chai').assert,
  loader = require('../index'),
  Datastore = require('nedb');


describe('historic_event_loader()', function () {

  var db = null;

  before(function (done) {
    this.timeout(8000)
    loader().done(function (ret) {
      db = ret;
      done();
    })
  });


  it('loads anything', function () {
      expect(db).to.not.be.a('null');
  });

  it('loads a nedb database', function () {
      expect(db).to.be.an('object');
  });

  it('has loaded some data', function (done) {
      this.timeout(8000)
      db.count({}, function(err, ct) {
        assert.isNull(err);
        expect(ct).to.be.gt(0);
        done();
      });
  });

  it('has loaded useful data', function (done) {
      this.timeout(8000)
      db.find({'year': '1888'}).limit(1).exec(function(err, docs) {
        assert.isNull(err);
        expect(docs[0]).to.be.a('object');
        expect(docs[0]).to.have.property('year')
        expect(docs[0].date.getYear()).to.equal(1888-1900)
        done()
      });
  });


});

