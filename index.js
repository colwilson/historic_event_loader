/*! load v0.0.0 - MIT license */

'use strict';

var Datastore = require('nedb');
var Promise = require("bluebird");

var load = function() {
  // console.log('loading historic data');
  return get_database().then(function (db) {
    return db;
  });
}

var get_database = function() {
  return new Promise(function(fulfill, reject) {
    var db = new Datastore({filename: './historic_events.nedb', autoload: true});
    db.count({}, function (err, ct) {
      if (err) reject(err)
      // console.log(ct);
      if(!ct) {
        // console.log('ingesting historic data from json file');
        return load_from_json().then(function (docs) {
          db.insert(docs);
          db.ensureIndex({ fieldName: 'date' }, function (err) {
            fulfill(db);
          });
        })
      } else {
        fulfill(db);
      };
    });
  });
}

var load_from_json = function() {
  return new Promise(function(fulfill, reject) {
    var raw = require('./events.json');
    var docs = []
    for (var key in raw.pages) {
      var page = raw.pages[key];
      for (var key in page.results) {
        var object = page.results[key];
        var year = object.year;
        var events = object.event;
        for (var key in events) {
          var event = events[key];  
          var split = event.split(/ [–-] /, 2);
          if (split.length == 2) {
            var ds = split[0];
            var ev = split[1];
            var matches = ev.match( /(.*) \(([bd]). (\d\d\d\d)\)$/ )
            var doc = {'year': year, 'date': new Date(Date.parse(ds + ', ' + year))};
            if (matches) {
              doc.text = matches[1];
              if (matches[2] =='b') {
                doc.type = 'death'
                doc.birth_year = matches[3];
                doc.death_year = year;
              } else {
                doc.type = 'birth'
                doc.birth_year = year;
                doc.death_year = matches[3];
              }
            } else {
              doc.text = ev;
              doc.type = 'event';
            }
            docs.push(doc);
          }
        }
      }
    }
    fulfill(docs);
  });
}

module.exports = load;

