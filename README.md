# historic_event_loader

Create a simple [nedb](https://raw.githubusercontent.com/louischatriot/nedb) full of historical data (acquired originall from Wikipedia).

It loads from the json datafile in the first instance and then persists to disk after that.

## Installation

Install with npm:

```
npm install --save historic_event_loader
```


## API

### historic_event_loader()
```
loader = require('../index'),

loader().done(function (db) {
  db.count({}, function(err, ct) {
    console.log(ct)
  });
})

```


## Testing

From the repo root:

```
npm install
npm test
```
